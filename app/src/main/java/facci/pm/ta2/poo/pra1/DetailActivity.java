package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;
import java.lang.System;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    TextView nomnbre, precio, descripcion;
    ImageView imagen;

    String dolar = "\u0024";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");

        nomnbre = (TextView)findViewById(R.id.nombre) ;
        precio =(TextView)findViewById(R.id.precio);
        descripcion = (TextView)findViewById(R.id.descripcion);
        imagen = (ImageView)findViewById(R.id.thumbnail);

        descripcion.setMovementMethod(LinkMovementMethod.getInstance());
        String object_id = getIntent().getStringExtra("object");


        // INICIO - CODE6

        DataQuery query = DataQuery.get("item"); query.getInBackground(object_id , new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {

                nomnbre.setText((String)object.get("name"));
                precio.setText((String)object.get("price") + " " + dolar);
                precio.setTextColor(Color.RED);
                descripcion.setText((String)object.get("description"));
                imagen.setImageBitmap((Bitmap) object.get("image"));

            if (e == null) {



            } else {
// Error
            }
            }
        });


        // FIN - CODE6

    }

}
